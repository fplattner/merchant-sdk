<?php

class OrderItemTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\OrderItem $orderItem */
	private $orderItem;

	public function setup ()
	{
		$this->orderItem = new \mapi\OrderItem();
	}

	public function tearDown ()
	{
		$this->orderItem = null;
	}

	public function testSetGetId ()
	{
		$this->orderItem->setId(123);
		$this->assertEquals(123, $this->orderItem->getId());
	}

	public function testSetGetProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->orderItem->setProductVariant($mockVariant);
		$this->assertEquals('test', $this->orderItem->getProductVariant()->getId());
	}

	public function testSetGetQuantity ()
	{
		$this->orderItem->setQuantity(2);
		$this->assertEquals(2, $this->orderItem->getQuantity());
	}

	public function testSetGetReservation ()
	{
		$mockReservation = $this->getMock('\\mapi\\Reservation', array('getId'));
		$mockReservation->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->orderItem->setReservation($mockReservation);
		$this->assertEquals('test', $this->orderItem->getReservation()->getId());
	}

	public function testPropertyAccessId ()
	{
		$this->orderItem->id = 123;
		$this->assertEquals(123, $this->orderItem->id);
	}

	public function testPropertyAccessProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->orderItem->product_variant = $mockVariant;
		$this->assertEquals('test', $this->orderItem->product_variant->getId());
	}

	public function testPropertyAccessQuantity ()
	{
		$this->orderItem->quantity = 2;
		$this->assertEquals(2, $this->orderItem->quantity);
	}

	public function testPropertyAccessReservation ()
	{
		$mockReservation = $this->getMock('\\mapi\\Reservation', array('getId'));
		$mockReservation->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->orderItem->reservation = $mockReservation;
		$this->assertEquals('test', $this->orderItem->reservation->getId());
	}
	
	public function testSetIdReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\OrderItem', $this->orderItem->setId(123));
	}

	public function testSetQuantityReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\OrderItem', $this->orderItem->setQuantity(123));
	}

	public function testSetProductVariantReturnType ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->assertInstanceOf('\\mapi\\OrderItem', $this->orderItem->setProductVariant($mockVariant));
	}

	public function testSetReservationReturnType ()
	{
		$mockReservation = $this->getMock('\\mapi\\Reservation');
		$this->assertInstanceOf('\\mapi\\OrderItem', $this->orderItem->setReservation($mockReservation));
	}

	public function testMassAssignment ()
	{
		$this->orderItem->properties = array(
			'order_item_id' => 1,
			'quantity' => 2,
			'product_variant_id' => 'variant',
			'reservation_id' => 'reservation',
			'collins_product_variant_id' => 'collins',
		);

		$this->assertEquals(1, $this->orderItem->id);
		$this->assertEquals(2, $this->orderItem->quantity);
		$this->assertEquals('collins', $this->orderItem->collins_product_variant_id);
		$this->assertInstanceOf('\\mapi\\ProductVariant', $this->orderItem->product_variant);
		$this->assertInstanceOf('\\mapi\\Reservation', $this->orderItem->reservation);
		$this->assertEquals('variant', $this->orderItem->product_variant->id);
		$this->assertEquals('reservation', $this->orderItem->reservation->id);
	}

	public function testGetJson ()
	{
		$this->orderItem->properties = array(
			'order_item_id' => 1,
			'quantity' => 2,
			'product_variant_id' => 'variant',
			'reservation_id' => 'reservation',
			'collins_product_variant_id' => 'collins',
		);

		$expectedJson = '{"reservation_id":"reservation","merchant_product_variant_id":"variant","quantity":2,"collins_product_variant_id":"collins","order_item_id":1}';

		$this->assertEquals($expectedJson, $this->orderItem->toJson());
	}
}