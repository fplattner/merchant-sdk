<?php

class CategoryTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Category $category */
	private $category;

	public function setUp ()
	{
		$this->category = new \mapi\Category();
	}

	public function tearDown ()
	{
		$this->category = null;
	}

	public function testSetGetId ()
	{
		$this->category->setId(23);
		$this->assertEquals(23, $this->category->getId());
	}

	public function testSetGetName ()
	{
		$this->category->setName('Hemden');
		$this->assertEquals('Hemden', $this->category->getName());
	}

	public function testGetSetParentId ()
	{
		$mockParent = $this->getMock('\\mapi\\Category', array('getId'));
		$mockParent->expects($this->once())
			->method('getId')
			->will($this->returnValue(123));

		$this->category->setParent($mockParent);
		$this->assertEquals(123, $this->category->getParent()->getId());
	}

	public function testPropertyAccessId ()
	{
		$this->category->id = 23;
		$this->assertEquals(23, $this->category->id);
	}

	public function testPropertyAccessName ()
	{
		$this->category->name = 'Hemden';
		$this->assertEquals('Hemden', $this->category->name);
	}

	public function testPropertyAccessChildren ()
	{
		$this->category->children = array(1,2,3,4);
		$this->assertEquals(array(1,2,3,4), $this->category->children);
	}

	public function testPropertyAccessParentId ()
	{
		$this->category->parent_id = 2;
		$this->assertEquals(2, $this->category->parent_id);
	}

	public function testReturnValueSetId ()
	{
		$this->assertInstanceOf('\\mapi\\Category', $this->category->setId(1));
	}

	public function testReturnValueSetName ()
	{
		$this->assertInstanceOf('\\mapi\\Category', $this->category->setName('name'));
	}

	public function testReturnValueSetParentId ()
	{
		$mockParent = $this->getMock('\\mapi\\Category');
		$this->assertInstanceOf('\\mapi\\Category', $this->category->setParent($mockParent));
	}

	public function testMassAssignment ()
	{
		$this->category->properties = array(
			'id' => 2,
			'name' => 'Hemden',
			'parent_id' => 1,
			'children' => array(1,3,4),
		);

		$this->assertEquals(2, $this->category->id);
		$this->assertEquals('Hemden', $this->category->name);
		$this->assertEquals(1, $this->category->parent->id);
		$this->assertEquals(3, $this->category->children[3]->id);
	}

	public function testLoad ()
	{
		$mockConnector = $this->getMock('\\mapi\\base\\Connector', array('request'));
		$mockConnector->expects($this->once())
			->method('request')
			->with($this->equalTo('GET'), $this->equalTo('categories'))
			->will($this->returnValue(array(200,'[{"parent_id":1,"id":2,"name":"Hemden","children":[3,4,5]}]')));

		$this->category = new \mapi\Category(null, $mockConnector);
		$this->category->setMapiBaseUrl('http://localhost/merchant')
			->setUsername('user')
			->setPassword('password');

		$categories = $this->category->loadAll();
		$this->assertCount(1, $categories);
		/* @var \mapi\Category $testCategory */
		$testCategory = $categories[0];
		$this->assertEquals(1, $testCategory->parent_id);
		$this->assertEquals(2, $testCategory->id);
		$this->assertEquals('Hemden', $testCategory->name);
		$this->assertEquals(array(4), $testCategory->children[1]->id);
	}
}
