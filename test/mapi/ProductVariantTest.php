<?php

class ProductVariantTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\ProductVariant $variant */
	private $variant;

	public function setUp ()
	{
		$this->variant = new \mapi\ProductVariant();
	}

	public function tearDown()
	{
		$this->variant = null;
	}

	public function testSetGetId ()
	{
		$this->variant->setId('variant');
		$this->assertEquals('variant', $this->variant->getId());
	}

	public function testSetGetPrice ()
	{
		$this->variant->setPrice(123);
		$this->assertEquals(123, $this->variant->getPrice());
	}

	public function testSetGetRetailPrice ()
	{
		$this->variant->setRetailPrice(123);
		$this->assertEquals(123, $this->variant->getRetailPrice());
	}

	public function testSetGetPurchasePrice ()
	{
		$this->variant->setPurchasePrice(123);
		$this->assertEquals(123, $this->variant->getPurchasePrice());
	}

	public function testSetGetRedPrice ()
	{
		$this->variant->setRedPrice(123);
		$this->assertEquals(123, $this->variant->getRedPrice());
	}

	public function testSetGetMinPurchase ()
	{
		$this->variant->setMinPurchase(123);
		$this->assertEquals(123, $this->variant->getMinPurchase());
	}

	public function testSetGetMaxPurchase ()
	{
		$this->variant->setMaxPurchase(123);
		$this->assertEquals(123, $this->variant->getMaxPurchase());
	}

	public function testSetGetCurrency ()
	{
		$this->variant->setCurrency('EUR');
		$this->assertEquals('EUR', $this->variant->getCurrency());
	}

	public function testSetGetEan ()
	{
		$this->variant->setEan('123456789');
		$this->assertEquals('123456789', $this->variant->getEan());
	}

	public function testSetGetDefault ()
	{
		$this->variant->setDefault(true);
		$this->assertEquals(true, $this->variant->getDefault());
	}

	public function testSetGetQuantity ()
	{
		$this->variant->setQuantity(2);
		$this->assertEquals(2, $this->variant->getQuantity());
	}

	public function testSetGetImages ()
	{
		$this->variant->setImages(array('http://images.com/image.jpg'));
		$this->assertEquals(array('http://images.com/image.jpg'), $this->variant->getImages());
	}

	public function testSetGetAttributes ()
	{
		$mockAttribute = $this->getMock('\\mapi\\Attribute', array('getName'));
		$mockAttribute->expects($this->once())
			->method('getName')
			->will($this->returnValue('123'));

		$this->variant->setAttributes(array($mockAttribute));
		/* @var \mapi\Attribute $testAttribute */
		$testAttribute = $this->variant->getAttributes()[0];
		$this->assertEquals('123', $testAttribute->getName());
	}

	public function testPropertyAccessId ()
	{
		$this->variant->id = 'variant';
		$this->assertEquals('variant', $this->variant->id);
	}

	public function testPropertyAccessPrice ()
	{
		$this->variant->price = 123;
		$this->assertEquals(123, $this->variant->price);
	}

	public function testPropertyAccessRetailPrice ()
	{
		$this->variant->retail_price = 123;
		$this->assertEquals(123, $this->variant->retail_price);
	}

	public function testPropertyAccessPurchasePrice ()
	{
		$this->variant->purchase_price = 123;
		$this->assertEquals(123, $this->variant->purchase_price);
	}

	public function testPropertyAccessRedPrice ()
	{
		$this->variant->red_price = 123;
		$this->assertEquals(123, $this->variant->red_price);
	}

	public function testPropertyAccessMinPurchase ()
	{
		$this->variant->min_purchase = 123;
		$this->assertEquals(123, $this->variant->min_purchase);
	}

	public function testPropertyAccessMaxPurchase ()
	{
		$this->variant->max_purchase = 123;
		$this->assertEquals(123, $this->variant->max_purchase);
	}

	public function testPropertyAccessCurrency ()
	{
		$this->variant->currency = 'EUR';
		$this->assertEquals('EUR', $this->variant->currency);
	}

	public function testPropertyAccessEan ()
	{
		$this->variant->ean = '123456789';
		$this->assertEquals('123456789', $this->variant->ean);
	}

	public function testPropertyAccessDefault ()
	{
		$this->variant->default = true;
		$this->assertEquals(true, $this->variant->default);
	}

	public function testPropertyAccessQuantity ()
	{
		$this->variant->quantity = 2;
		$this->assertEquals(2, $this->variant->quantity);
	}

	public function testPropertyAccessImages ()
	{
		$this->variant->images = array('http://images.com/image.jpg');
		$this->assertEquals(array('http://images.com/image.jpg'), $this->variant->images);
	}

	public function testPropertyAccessAttributes ()
	{
		$mockAttribute = $this->getMock('\\mapi\\Attribute', array('getName'));
		$mockAttribute->expects($this->once())
			->method('getName')
			->will($this->returnValue('123'));

		$this->variant->attributes = array($mockAttribute);
		/* @var \mapi\Attribute $testAttribute */
		$testAttribute = $this->variant->attributes[0];
		$this->assertEquals('123', $testAttribute->getName());
	}

	public function testMassAssignment ()
	{
		$this->variant->properties = array(
			'id' => 'variant',
			'price' => 123,
			'red_price' => 456,
			'purchase_price' => 789,
			'retail_price' => 1243,
			'min_purchase' => 1,
			'max_purchase' => 5,
			'currency' => 'EUR',
			'ean' => '123456789',
			'default' => true,
//			'active' => true,
			'quantity' => 5,
			'images' => array(
				'http://images.com/image.jpg',
			),
			'attributes' => array(
				'size' => array(
					'S',
				),
			),
		);

		$this->assertEquals('variant', $this->variant->id);
		$this->assertEquals(123, $this->variant->price);
		$this->assertEquals(456, $this->variant->red_price);
		$this->assertEquals(789, $this->variant->purchase_price);
		$this->assertEquals(1243, $this->variant->retail_price);
		$this->assertEquals(1, $this->variant->min_purchase);
		$this->assertEquals(5, $this->variant->max_purchase);
		$this->assertEquals('EUR', $this->variant->currency);
		$this->assertEquals('123456789', $this->variant->ean);
		$this->assertEquals(true, $this->variant->default);
//		$this->assertEquals(true, $this->variant->active);
		$this->assertEquals(5, $this->variant->quantity);
		$this->assertEquals(array('http://images.com/image.jpg'), $this->variant->images);
		/* @var \mapi\Attribute $attribute */
		$testAttribute = $this->variant->attributes[0];
		$this->assertEquals('size', $testAttribute->name);
		$this->assertEquals(array('S'), $testAttribute->values);
	}

	public function testToJson ()
	{
		$this->variant->properties = array(
			'id' => 'variant',
			'price' => 123,
			'red_price' => 456,
			'purchase_price' => 789,
			'retail_price' => 1243,
			'min_purchase' => 1,
			'max_purchase' => 5,
			'currency' => 'EUR',
			'ean' => '123456789',
			'default' => true,
//			'active' => true,
			'quantity' => 5,
			'images' => array(
				'http://images.com/image.jpg',
			),
			'attributes' => array(
				'size' => array(
					'S',
				),
			),
		);

		$expectedJson = '{"price":123,"retail_price":1243,"red_price":456,"purchase_price":789,"min_purchase":1,"max_purchase":5,"currency":"EUR","ean":"123456789","default":true,"quantity":5,"images":["http:\/\/images.com\/image.jpg"],"attributes":{"size":["S"]},"id":"variant"}';

		$this->assertEquals($expectedJson, $this->variant->toJson());
	}
}
