<?php

class ReservationTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Reservation $reservation */
	private $reservation;

	public function setUp ()
	{
		$this->reservation = new \mapi\Reservation();
	}

	public function tearDown ()
	{
		$this->reservation = null;
	}

	public function testSetGetProductVariant ()
	{
		$testValue   = 'zrigmvklfdmvlfd'; // some random string
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->any())
			->method('getId')
			->will($this->returnValue($testValue));

		$this->reservation->setProductVariant($mockVariant);
		$this->assertEquals($testValue, $this->reservation->getProductVariant()->getId());
	}

	public function testSetGetQuantity ()
	{
		$testValue = 145261; // some number

		$this->reservation->setQuantity($testValue);
		$this->assertEquals($testValue, $this->reservation->getQuantity());
	}

	public function testSetGetId ()
	{
		$testValue = 'jgiofdsag';

		$this->reservation->setId($testValue);
		$this->assertEquals($testValue, $this->reservation->getId());
	}

	public function testSetVariantReturnType ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->assertInstanceOf('\\mapi\\Reservation', $this->reservation->setProductVariant($mockVariant));
	}

	public function testSetQuantityReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Reservation', $this->reservation->setQuantity(null));
	}

	public function testSetIdReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Reservation', $this->reservation->setId('ghidag'));
	}

	public function testPropertyAccessQuantity ()
	{
		$this->reservation->quantity = 5;
		$this->assertEquals(5, $this->reservation->quantity);
	}

	public function testPropertyAccessProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->reservation->product_variant = $mockVariant;
		$this->assertEquals($mockVariant, $this->reservation->product_variant);
	}

	public function testPropertyAccessId ()
	{
		$testValue = 'jfioag';
		$this->reservation->id = $testValue;
		$this->assertEquals($testValue, $this->reservation->id);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetQuantityWrongValueException ()
	{
		$this->reservation->setQuantity('gfhsjo');
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetProductVariantWrongValueException ()
	{
		$this->reservation->setProductVariant('hksdövckv');
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetIdWrongValueException ()
	{
		$this->reservation->setId(true);
	}

	public function testGetJson ()
	{
		$reservationId = 'fuifdls';
		$productVariantId = 'hdkfasdkjf';
		$expectedJson = '{"product_variant_id":"hdkfasdkjf","quantity":2,"reservation_id":"fuifdls"}';

		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue($productVariantId));

		$this->reservation->setProductVariant($mockVariant);
		$this->reservation->setQuantity(2);
		$this->reservation->setId($reservationId);
		$this->assertEquals($expectedJson, $this->reservation->toJson());
	}

	public function testMassAssignment ()
	{
		$testData = array(
			'reservation_id' => 'hukdsf',
			'quantity' => 2,
			'product_variant_id' => 'fiosafj',
		);

		$this->reservation->properties = $testData;

		$this->assertEquals('fiosafj', $this->reservation->getProductVariant()->getId());
		$this->assertEquals(2, $this->reservation->quantity);
		$this->assertEquals('hukdsf', $this->reservation->id);
	}

	public function testGetSetPropertyAccessQuantity ()
	{
		$this->reservation->quantity = 5;
		$this->assertEquals(5, $this->reservation->getQuantity());

		$this->reservation->setQuantity(3);
		$this->assertEquals(3, $this->reservation->quantity);
	}
}
