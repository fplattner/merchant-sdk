<?php

class BrandTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Brand $brand */
	private $brand;

	public function setUp ()
	{
		$this->brand = new \mapi\Brand();
	}

	public function tearDown()
	{
		$this->brand = null;
	}

	public function testSetGetName ()
	{
		$this->brand->setName('brand');
		$this->assertEquals('brand', $this->brand->getName());
	}

	public function testSetGetId ()
	{
		$this->brand->setId(6784);
		$this->assertEquals(6784, $this->brand->getId());
	}

	public function testPropertyAccessName ()
	{
		$this->brand->name = 'brand';
		$this->assertEquals('brand', $this->brand->name);
	}

	public function testPropertyAccessId ()
	{
		$this->brand->id = 345;
		$this->assertEquals(345, $this->brand->id);
	}

	public function testMassAssignment ()
	{
		$this->brand->properties = array(
			'name' => 'brand',
			'id' => 123,
		);

		$this->assertEquals('brand', $this->brand->name);
		$this->assertEquals(123, $this->brand->id);
	}

	public function testGetJson ()
	{
		$this->brand->properties = array(
			'name' => 'brand',
			'id' => 123,
		);

		$expectedJson = '{"name":"brand","id":123}';

		$this->assertEquals($expectedJson, $this->brand->toJson());
	}

	public function testLoad ()
	{
		$headers = array(
			'Authorization: Basic ' . base64_encode('user:password'),
		);

		$mockConnector = $this->getMock('\\mapi\\base\\Connector', array('request'));
		$mockConnector->expects($this->once())
			->method('request')
			->with($this->equalTo('GET'), $this->equalTo('http://localhost/merchant/brands'), $this->equalTo($headers))
			->will($this->returnValue(array(200, '[{"name":"brand","id":123}]')));

		$this->brand = new \mapi\Brand(null, $mockConnector);
		$this->brand->setMapiBaseUrl('http://localhost/merchant')
			->setUsername('user')
			->setPassword('password');

		$brands = $this->brand->loadAll();
		$this->assertCount(1, $brands);
		/* @var \mapi\Brand $testBrand */
		$testBrand = $brands[0];
		$this->assertEquals('brand', $testBrand->name);
		$this->assertEquals(123, $testBrand->id);
	}
}