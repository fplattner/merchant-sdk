<?php

class OrderTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Order $order */
	private $order;

	public function setUp ()
	{
		$this->order = new \mapi\Order();
	}

	public function tearDown ()
	{
		$this->order = null;
	}

	public function testSetGetCustomerId ()
	{
		$this->order->setCustomerId(123);
		$this->assertEquals(123, $this->order->getCustomerId());
	}

	public function testSetGetItems ()
	{
		$mockItem = $this->getMock('\\mapi\\OrderItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue(123));

		$this->order->setItems(array($mockItem));
		/* @var \mapi\OrderItem $item */
		$item = $this->order->getItems()[0];
		$this->assertEquals(123, $item->getId());
	}

	public function testSetGetAddressShipping ()
	{
		$mockAddress = $this->getMock('\\mapi\\AddressShipping', array('getFirstname'));
		$mockAddress->expects($this->once())
			->method('getFirstname')
			->will($this->returnValue('Tim'));

		$this->order->setAddressShipping($mockAddress);
		$this->assertEquals('Tim', $this->order->getAddressShipping()->getFirstname());
	}

	public function testPropertyAccessCustomerId ()
	{
		$this->order->customer_id = 123;
		$this->assertEquals(123, $this->order->customer_id);
	}

	public function testPropertyAccessItems ()
	{
		$mockItem = $this->getMock('\\mapi\\OrderItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue(123));

		$this->order->items = array($mockItem);
		/* @var \mapi\OrderItem $item */
		$item = $this->order->items[0];
		$this->assertEquals(123, $item->getId());
	}

	public function testPropertyAccessAddressShipping ()
	{
		$mockAddress = $this->getMock('\\mapi\\AddressShipping', array('getFirstname'));
		$mockAddress->expects($this->once())
			->method('getFirstname')
			->will($this->returnValue('Tim'));

		$this->order->address_shipping = $mockAddress;
		$this->assertEquals('Tim', $this->order->address_shipping->getFirstname());
	}

	public function testSetAddressShippingReturnType ()
	{
		$mockAddress = $this->getMock('\\mapi\\AddressShipping', array('getFirstname'));
		$this->assertInstanceOf('\\mapi\\Order', $this->order->setAddressShipping($mockAddress));
	}

	public function testSetCustomerIdReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Order', $this->order->setCustomerId(123));
	}

	public function testSetItemsReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Order', $this->order->setItems(array()));
	}

	public function testMassAssignment ()
	{
		$this->order->properties = array(
			'customer_id' => 123,
			'items' => array(
				array(
					'order_item_id' => 1,
					'quantity' => 2,
					'product_variant_id' => 'variant',
					'reservation_id' => 'reservation',
					'collins_product_variant_id' => 'collins',
				),
			),
			'address_shipping' => array(
				'company'      => '',
				'firstname'    => 'Tim',
				'lastname'     => 'Tester',
				'salutation'   => 'm',
				'street'       => 'My Street',
				'number'       => '13b',
				'additional'   => '',
				'zip_code'     => '42345',
				'city'         => 'MyCity',
				'country_code' => 'DEU',
				'phone'        => '65874359745',
				'department'   => '',
			),
		);

		$this->assertEquals(123, $this->order->customer_id);
		$this->assertInstanceof('\\mapi\\AddressShipping', $this->order->address_shipping);
		$this->assertEquals('Tim', $this->order->address_shipping->firstname);
		$this->assertCount(1, $this->order->items);
		/* @var \mapi\OrderItem $testItem */
		$testItem = $this->order->items[0];
		$this->assertInstanceOf('\\mapi\\OrderItem', $testItem);
		$this->assertEquals(1, $testItem->id);
	}

	public function testGetJson ()
	{
		$this->order->properties = array(
			'id' => 'test',
			'customer_id' => 123,
			'items' => array(
				array(
					'order_item_id' => 1,
					'quantity' => 2,
					'product_variant_id' => 'variant',
					'reservation_id' => 'reservation',
					'collins_product_variant_id' => 'collins',
				),
			),
			'address_shipping' => array(
				'company'      => '',
				'firstname'    => 'Tim',
				'lastname'     => 'Tester',
				'salutation'   => 'm',
				'street'       => 'My Street',
				'number'       => '13b',
				'additional'   => '',
				'zip_code'     => '42345',
				'city'         => 'MyCity',
				'country_code' => 'DEU',
				'phone'        => '65874359745',
				'department'   => '',
			),
		);

		$expectedJson = '{"id":"test","customer_id":123,"address_shipping":{"company":"","firstname":"Tim","lastname":"Tester","salutation":"m","street":"My Street","number":"13b","additional":"","zip_code":"42345","city":"MyCity","country_code":"DEU","phone":"65874359745","department":""},"items":[{"reservation_id":"reservation","merchant_product_variant_id":"variant","quantity":2,"collins_product_variant_id":"collins","order_item_id":1}]}';

		$this->assertEquals($expectedJson, $this->order->toJson());
	}
}
