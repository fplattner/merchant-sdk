<?php

class ShipmentTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Shipment $shipment */
	private $shipment;

	public function setUp ()
	{
		$this->shipment = new \mapi\Shipment();
	}

	public function tearDown ()
	{
		$this->shipment = null;
	}

	public function testSetGetId ()
	{
		$this->shipment->setId('test');
		$this->assertEquals('test', $this->shipment->getId());
	}

	public function testSetGetOrder ()
	{
		$mockOrder = $this->getMock('\\mapi\\Order', array('getId'));
		$mockOrder->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->shipment->setOrder($mockOrder);
		$this->assertEquals('test', $this->shipment->getOrder()->getId());
	}

	public function testSetGetShipmentKey ()
	{
		$this->shipment->setShipmentKey('test');
		$this->assertEquals('test', $this->shipment->getShipmentKey());
	}

	public function testSetGetCarrier ()
	{
		$this->shipment->setCarrier('test');
		$this->assertEquals('test', $this->shipment->getCarrier());
	}

	public function testSetGetDeliveryDate ()
	{
		$this->shipment->setDeliveryDate('2013-12-04');
		$this->assertEquals('2013-12-04T00:00:00', $this->shipment->getDeliveryDate());
	}

	public function testSetGetReturnDocumentUrl ()
	{
		$this->shipment->setReturnDocumentUrl('test');
		$this->assertEquals('test', $this->shipment->getReturnDocumentUrl());
	}

	public function testSetGetItems ()
	{
		$mockItem = $this->getMock('\\mapi\\ShipmentItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->shipment->setItems(array($mockItem));
		/* @var \mapi\ShipmentItem $testItem */
		$testItem = $this->shipment->getItems()[0];
		$this->assertEquals('test', $testItem->getId());
	}

	public function testPropertyAccessId ()
	{
		$this->shipment->id = 'test';
		$this->assertEquals('test', $this->shipment->id);
	}

	public function testPropertyAccessOrder ()
	{
		$mockOrder = $this->getMock('\\mapi\\Order', array('getId'));
		$mockOrder->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->shipment->order = $mockOrder;
		$this->assertEquals('test', $this->shipment->order->getId());
	}

	public function testPropertyAccessShipmentKey ()
	{
		$this->shipment->shipment_key = 'test';
		$this->assertEquals('test', $this->shipment->shipment_key);
	}

	public function testPropertyAccessCarrier ()
	{
		$this->shipment->carrier = 'test';
		$this->assertEquals('test', $this->shipment->carrier);
	}

	public function testPropertyAccessDeliveryDate ()
	{
		$this->shipment->delivery_date = '2013-12-04';
		$this->assertEquals('2013-12-04T00:00:00', $this->shipment->delivery_date);
	}

	public function testPropertyAccessReturnDocumentUrl ()
	{
		$this->shipment->return_document_url = 'test';
		$this->assertEquals('test', $this->shipment->return_document_url);
	}

	public function testPropertyAccessItems ()
	{
		$mockItem = $this->getMock('\\mapi\\ShipmentItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue('test'));

		$this->shipment->items = array($mockItem);
		/* @var \mapi\ShipmentItem $testItem */
		$testItem = $this->shipment->items[0];
		$this->assertEquals('test', $testItem->getId());
	}
	
	public function testSetIdReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setId('test'));
	}

	public function testSetOrderReturnType ()
	{
		$mockOrder = $this->getMock('\\mapi\\Order', array('getId'));
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setOrder($mockOrder));
	}

	public function testSetShipment_keyReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setShipmentKey('test'));
	}

	public function testSetCarrierReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setCarrier('test'));
	}

	public function testSetDelivery_dateReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setDeliveryDate('test'));
	}

	public function testSetReturn_document_urlReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setReturnDocumentUrl('test'));
	}

	public function testSetItemsReturnType ()
	{
		$mockItem = $this->getMock('\\mapi\\ShipmentItem', array('getId'));
		$this->assertInstanceOf('\\mapi\\Shipment', $this->shipment->setItems(array($mockItem)));
	}

	public function testMassAssignment ()
	{
		$this->shipment->properties = array(
			'id' => 'testId',
			'order_id' => 'testOid',
			'shipment_key' => 'shipmentKey',
			'carrier' => 'carrier',
			'delivery_date' => '2013-12-05',
			'items' => array(
				array(
					'order_item_id' => 123,
					'product_variant_id' => 'variant',
					'return_key' => 'key',
					'return_letter' => 'A',
					'deliverable' => true,
				),
			),
			'return_document_url' => '',
		);

		$this->assertEquals('testId', $this->shipment->id);
		$this->assertInstanceOf('\\mapi\\Order', $this->shipment->order);
		$this->assertEquals('testOid', $this->shipment->order->id);
		$this->assertEquals('shipmentKey', $this->shipment->shipment_key);
		$this->assertEquals('carrier', $this->shipment->carrier);
		$this->assertEquals('2013-12-05T00:00:00', $this->shipment->delivery_date);
		$this->assertCount(1, $this->shipment->items);
		$testItem = $this->shipment->items[0];
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $testItem);
		$this->assertEquals('key', $testItem->return_key);
		$this->assertEquals('', $this->shipment->return_document_url);
	}
	
	public function testGetJson ()
	{
		$this->shipment->properties = array(
			'id' => 'testId',
			'order_id' => 'testOid',
			'shipment_key' => 'shipmentKey',
			'carrier' => 'carrier',
			'delivery_date' => '2013-12-05',
			'items' => array(
				array(
					'order_item_id' => 123,
					'product_variant_id' => 'variant',
					'return_key' => 'key',
					'return_letter' => 'A',
					'deliverable' => true,
				),
			),
			'return_document_url' => '',
		);
		
		$expectedJson = '{"shipment_key":"shipmentKey","carrier":"carrier","delivery_date":"2013-12-05T00:00:00","return_document_url":"","items":[{"order_item_id":123,"product_variant_id":"variant","return_key":"key","return_letter":"A","deliverable":true}],"id":"testId","order_id":"testOid"}';
		$expectedJsonNoId = '{"shipment_key":"shipmentKey","carrier":"carrier","delivery_date":"2013-12-05T00:00:00","return_document_url":"","items":[{"order_item_id":123,"product_variant_id":"variant","return_key":"key","return_letter":"A","deliverable":true}]}';

		$this->assertEquals($expectedJson, $this->shipment->toJson());
		$this->assertEquals($expectedJsonNoId, $this->shipment->toJson(false));
	}
}
