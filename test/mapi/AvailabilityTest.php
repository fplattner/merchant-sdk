<?php

class AvailabilityTest extends PHPUnit_Framework_Testcase
{
	/* @var \mapi\Availability $availability */
	private $availability;

	public function setUp()
	{
		$this->availability = new \mapi\Availability();
	}

	public function tearDown()
	{
		$this->availability = null;
	}

	public function testSetGetProductVariant ()
	{
		$testValue   = 'zrigmvklfdmvlfd'; // some random string
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->any())
			->method('getId')
			->will($this->returnValue($testValue));

		$this->availability->setProductVariant($mockVariant);
		$this->assertEquals($testValue, $this->availability->getProductVariant()->getId());
	}

	public function testSetGetQuantity ()
	{
		$testValue = 145261; // some number

		$this->availability->setQuantity($testValue);
		$this->assertEquals($testValue, $this->availability->getQuantity());
	}

	public function testSetVariantReturnType ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->assertInstanceOf('\\mapi\\Availability', $this->availability->setProductVariant($mockVariant));
	}

	public function testSetQuantityReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\Availability', $this->availability->setQuantity(null));
	}

	public function testPropertyAccessQuantity ()
	{
		$this->availability->quantity = 5;
		$this->assertEquals(5, $this->availability->quantity);
	}

	public function testPropertyAccessProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->availability->product_variant = $mockVariant;
		$this->assertEquals($mockVariant, $this->availability->product_variant);
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetQuantityWrongValueException ()
	{
		$this->availability->setQuantity('gfhsjo');
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetProductVariantWrongValueException ()
	{
		$this->availability->setProductVariant('hksdövckv');
	}

	public function testGetJson ()
	{
		$testValue = 'fuifdls';
		$expectedJson = '{"quantity":2,"id":"fuifdls"}';

		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue($testValue));

		$this->availability->setProductVariant($mockVariant);
		$this->availability->setQuantity(2);
		$this->assertEquals($expectedJson, $this->availability->toJson());
	}

	public function testMassAssignment ()
	{
		$testData = array(
			'id' => 'hukdsf',
			'quantity' => 2,
		);

		$this->availability->properties = $testData;

		$this->assertEquals('hukdsf', $this->availability->getProductVariant()->getId());
		$this->assertEquals(2, $this->availability->quantity);
	}

	public function testGetSetPropertyAccessQuantity ()
	{
		$this->availability->quantity = 5;
		$this->assertEquals(5, $this->availability->getQuantity());

		$this->availability->setQuantity(3);
		$this->assertEquals(3, $this->availability->quantity);
	}
}
