<?php


class AttributeTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Attribute $attribute */
	private $attribute;

	public function setUp ()
	{
		$this->attribute = new \mapi\Attribute();
	}

	public function tearDown ()
	{
		$this->attribute = null;
	}

	public function testSetGetName ()
	{
		$this->attribute->setName('name');
		$this->assertEquals('name', $this->attribute->getName());
	}

	public function testSetGetValue ()
	{
		$this->attribute->setValues(array('value'));
		$this->assertEquals(array('value'), $this->attribute->getValues());
	}

	public function testPropertyAccessName ()
	{
		$this->attribute->name = 'name';
		$this->assertEquals('name', $this->attribute->name);
	}

	public function testPropertyAccessValue ()
	{
		$this->attribute->values = array('value');
		$this->assertEquals(array('value'), $this->attribute->values);
	}

	public function testMassAssignment ()
	{
		$this->attribute->properties = array(
			'name' => 'name',
			'values' => array('value'),
		);

		$this->assertEquals('name', $this->attribute->name);
		$this->assertEquals(array('value'), $this->attribute->values);
	}

	public function testGetJson ()
	{
		$testValues = array(
			'name' => 'name',
			'values' => array('value'),
		);

		$expectedJson = '{"name":["value"]}';

		$this->attribute->properties = $testValues;

		$this->assertEquals($expectedJson, $this->attribute->toJson());
	}

	public function testLoad ()
	{
		$authorization = 'Authorization: Basic ' . base64_encode('user:password');

		$mockConnector = $this->getMock('\\mapi\\base\\Connector', array('request'));
		$mockConnector->expects($this->once())
			->method('request')
			->with($this->equalTo('GET'), $this->equalTo('http://localhost/merchant/attributes'), $this->equalTo(array($authorization)))
			->will($this->returnValue(array(200,'{"clothing_unisex_int":["S","M","L","XL"]}')));

		$this->attribute = new \mapi\Attribute(null, $mockConnector);
		$this->attribute->setMapiBaseUrl('http://localhost/merchant');
		$this->attribute->setUsername('user');
		$this->attribute->setPassword('password');
		$attributes = $this->attribute->loadAll();

		$this->assertCount(1, $attributes);
		/* @var \mapi\Attribute $testAttribute */
		$testAttribute = $attributes[0];
		$this->assertEquals('clothing_unisex_int', $testAttribute->name);
		$this->assertEquals(array('S', 'M', 'L', 'XL'), $testAttribute->values);
	}
}
