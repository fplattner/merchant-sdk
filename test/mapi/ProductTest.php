<?php

class ProductTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\Product $product */
	private $product;

	public function setUp ()
	{
		$this->product = new \mapi\Product();
	}

	public function tearDown ()
	{
		$this->product = null;
	}

	public function testSetGetId ()
	{
		$this->product->setId('product');
		$this->assertEquals('product', $this->product->getId());
	}

	public function testSetGetName ()
	{
		$this->product->setName('product');
		$this->assertEquals('product', $this->product->getName());
	}

	public function testSetGetDescriptionShort ()
	{
		$this->product->setDescriptionShort('product');
		$this->assertEquals('product', $this->product->getDescriptionShort());
	}

	public function testSetGetDescriptionLong ()
	{
		$this->product->setDescriptionLong('product');
		$this->assertEquals('product', $this->product->getDescriptionLong());
	}

	public function testSetGetBrand ()
	{
		$mockBrand = $this->getMock('\\mapi\\Brand', array('getId'));
		$mockBrand->expects($this->once())
			->method('getId')
			->will($this->returnValue(12));

		$this->product->setBrand($mockBrand);
		$this->assertEquals(12, $this->product->getBrand()->getId());
	}


	public function testSetGetProductVariants ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('variant'));

		$this->product->setVariants(array($mockVariant));
		/* @var \mapi\ProductVariant $testVariant */
		$testVariant = $this->product->getVariants()[0];
		$this->assertEquals('variant', $testVariant->getId());
	}

	public function testSetGetCategories ()
	{
		$mockCategory = $this->getMock('\\mapi\\Category', array('getId'));
		$mockCategory->expects($this->once())
			->method('getId')
			->will($this->returnValue(12));

		$this->product->setCategories(array($mockCategory));
		/* @var \mapi\Category $testCategory */
		$testCategory = $this->product->getCategories()[0];
		$this->assertEquals(12, $testCategory->getId());
	}

	public function testSetGetImages ()
	{
		$this->product->setImages(array('http://images.com/image.jpg'));
		$this->assertEquals(array('http://images.com/image.jpg'), $this->product->getImages());
	}


	public function testPropertyAccessId ()
	{
		$this->product->id = 'product';
		$this->assertEquals('product', $this->product->id);
	}

	public function testPropertyAccessName ()
	{
		$this->product->name = 'product';
		$this->assertEquals('product', $this->product->name);
	}

	public function testPropertyAccessDescriptionShort ()
	{
		$this->product->description_short = 'product';
		$this->assertEquals('product', $this->product->description_short);
	}

	public function testPropertyAccessDescriptionLong ()
	{
		$this->product->description_long = 'product';
		$this->assertEquals('product', $this->product->description_long);
	}

	public function testPropertyAccessBrand ()
	{
		$mockBrand = $this->getMock('\\mapi\\Brand', array('getId'));
		$mockBrand->expects($this->once())
			->method('getId')
			->will($this->returnValue(12));

		$this->product->brand = $mockBrand;
		$this->assertEquals(12, $this->product->brand->id);
	}

	public function testPropertyAccessProductVariants ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('variant'));

		$this->product->variants = array($mockVariant);
		/* @var \mapi\ProductVariant $testVariant */
		$testVariant = $this->product->variants[0];
		$this->assertEquals('variant', $testVariant->getId());
	}

	public function testPropertyAccessCategories ()
	{
		$mockCategory = $this->getMock('\\mapi\\Category', array('getId'));
		$mockCategory->expects($this->once())
			->method('getId')
			->will($this->returnValue(12));

		$this->product->categories = array($mockCategory);
		/* @var \mapi\Category $testCategory */
		$testCategory = $this->product->categories[0];
		$this->assertEquals(12, $testCategory->getId());
	}

	public function testPropertyAccessImages ()
	{
		$this->product->images = array('http://images.com/image.jpg');
		$this->assertEquals(array('http://images.com/image.jpg'), $this->product->images);
	}


	public function testMassAssignment ()
	{
		$this->product->properties = array(
			'id' => 'product',
			'name' => 'productName',
			'description_short' => 'lalala',
			'description_long' => 'rererere',
			'brand_id' => 12,
			'active' => true,
			'variants' => array(
				array(
					'id' => 'variant',
					'price' => 123,
					'red_price' => 456,
					'purchase_price' => 789,
					'retail_price' => 1243,
					'min_purchase' => 1,
					'max_purchase' => 5,
					'currency' => 'EUR',
					'ean' => '123456789',
					'default' => true,
					'active' => true,
					'quantity' => 5,
					'images' => array(
						'http://images.com/image.jpg',
					),
					'attributes' => array(
						'size' => array(
							'S',
						),
					),
				),
			),
			'categories' => array(
				12,13,14,
			),
			'images' => array(
				'http://images.com/image.jpg',
			)
		);

		$this->assertEquals('product', $this->product->id);
		$this->assertEquals('productName', $this->product->name);
		$this->assertEquals('lalala', $this->product->description_short);
		$this->assertEquals('rererere', $this->product->description_long);
		$this->assertEquals(array('http://images.com/image.jpg'), $this->product->images);
		$this->assertCount(1, $this->product->variants);
		/* @var \mapi\ProductVariant $testVariant */
		$testVariant = $this->product->variants[0];
		$this->assertEquals('variant', $testVariant->id);
		$this->assertCount(3, $this->product->categories);
		$categories = $this->product->categories;
		$this->assertInstanceOf('\\mapi\\Category', $categories[0]);
		$this->assertInstanceOf('\\mapi\\Category', $categories[1]);
		$this->assertInstanceOf('\\mapi\\Category', $categories[2]);
		$this->assertEquals(12, $categories[0]->id);
		$this->assertEquals(13, $categories[1]->id);
		$this->assertEquals(14, $categories[2]->id);
		$this->assertInstanceOf('\\mapi\\Brand', $this->product->brand);
		$this->assertEquals(12, $this->product->brand->id);
		$this->assertEquals(true, $this->product->active);
	}
	
	public function testToJson ()
	{
		$this->product->properties = array(
			'id' => 'product',
			'name' => 'productName',
			'description_short' => 'lalala',
			'description_long' => 'rererere',
			'brand_id' => 12,
			'active' => true,
			'variants' => array(
				array(
					'id' => 'variant',
					'price' => 123,
					'red_price' => 456,
					'purchase_price' => 789,
					'retail_price' => 1243,
					'min_purchase' => 1,
					'max_purchase' => 5,
					'currency' => 'EUR',
					'ean' => '123456789',
					'default' => true,
					'active' => true,
					'quantity' => 5,
					'images' => array(
						'http://images.com/image.jpg',
					),
					'attributes' => array(
						'size' => array(
							'S',
						),
					),
				),
			),
			'categories' => array(
				12,13,14,
			),
			'images' => array(
				'http://images.com/image.jpg',
			)
		);

		$expectedJson = '{"name":"productName","description_short":"lalala","description_long":"rererere","brand_id":12,"active":true,"images":["http:\/\/images.com\/image.jpg"],"variants":[{"price":123,"retail_price":1243,"red_price":456,"purchase_price":789,"min_purchase":1,"max_purchase":5,"currency":"EUR","ean":"123456789","default":true,"quantity":5,"images":["http:\/\/images.com\/image.jpg"],"attributes":{"size":["S"]},"id":"variant"}],"categories":[12,13,14],"id":"product"}';

		$this->assertEquals($expectedJson, $this->product->toJson());
	}
}
