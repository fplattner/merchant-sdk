<?php

class ShipmentItem extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\ShipmentItem $shipmentItem */
	private $shipmentItem;

	public function setUp ()
	{
		$this->shipmentItem = new \mapi\ShipmentItem();
	}

	public function tearDown ()
	{
		$this->shipmentItem = null;
	}

	public function testSetGetReturnKey ()
	{
		$this->shipmentItem->setReturnKey('1234');
		$this->assertEquals('1234', $this->shipmentItem->getReturnKey());
	}

	public function testSetGetReturnLetter ()
	{
		$this->shipmentItem->setReturnLetter('1234');
		$this->assertEquals('1234', $this->shipmentItem->getReturnLetter());
	}

	public function testSetGetDeliverable ()
	{
		$this->shipmentItem->setDeliverable(true);
		$this->assertEquals(true, $this->shipmentItem->getDeliverable());
	}

	public function testSetGetProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('variant'));

		$this->shipmentItem->setProductVariant($mockVariant);
		$this->assertEquals('variant', $this->shipmentItem->getProductVariant()->getId());
	}

	public function testSetGetOrderItem ()
	{
		$mockItem = $this->getMock('\\mapi\\OrderItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue('orderItem'));

		$this->shipmentItem->setOrderItem($mockItem);
		$this->assertEquals('orderItem', $this->shipmentItem->getOrderItem()->getId());
	}

	public function testPropertyAccessReturnKey ()
	{
		$this->shipmentItem->return_key = '1234';
		$this->assertEquals('1234', $this->shipmentItem->return_key);
	}

	public function testPropertyAccessReturnLetter ()
	{
		$this->shipmentItem->return_letter = '1234';
		$this->assertEquals('1234', $this->shipmentItem->return_letter);
	}

	public function testPropertyAccessDeliverable ()
	{
		$this->shipmentItem->deliverable = true;
		$this->assertEquals(true, $this->shipmentItem->deliverable);
	}

	public function testPropertyAccessProductVariant ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant', array('getId'));
		$mockVariant->expects($this->once())
			->method('getId')
			->will($this->returnValue('variant'));

		$this->shipmentItem->product_variant = $mockVariant;
		$this->assertEquals('variant', $this->shipmentItem->product_variant->getId());
	}

	public function testPropertyAccessOrderItem ()
	{
		$mockItem = $this->getMock('\\mapi\\OrderItem', array('getId'));
		$mockItem->expects($this->once())
			->method('getId')
			->will($this->returnValue('orderItem'));

		$this->shipmentItem->order_item = $mockItem;
		$this->assertEquals('orderItem', $this->shipmentItem->order_item->getId());
	}

	public function testSetReturnKeyReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $this->shipmentItem->setReturnKey('1234'));
	}

	public function testSetReturnLetterReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $this->shipmentItem->setReturnLetter('A'));
	}

	public function testSetDeliverableReturnType ()
	{
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $this->shipmentItem->setDeliverable(true));
	}

	public function testSetProductVariantReturnType ()
	{
		$mockVariant = $this->getMock('\\mapi\\ProductVariant');
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $this->shipmentItem->setProductVariant($mockVariant));
	}

	public function testSetOrderItemReturnType ()
	{
		$mockItem = $this->getMock('\\mapi\\OrderItem');
		$this->assertInstanceOf('\\mapi\\ShipmentItem', $this->shipmentItem->setOrderItem($mockItem));
	}

	public function testMassAssignment ()
	{
		$this->shipmentItem->properties = array(
			'order_item_id' => 1234,
			'product_variant_id' => 'variant',
			'return_key' => '1234456',
			'return_letter' => 'A',
			'deliverable' => true,
		);

		$this->assertInstanceOf('\\mapi\\OrderItem', $this->shipmentItem->order_item);
		$this->assertEquals(1234, $this->shipmentItem->order_item->id);
		$this->assertInstanceOf('\\mapi\\ProductVariant', $this->shipmentItem->product_variant);
		$this->assertEquals('variant', $this->shipmentItem->product_variant->id);
		$this->assertEquals('1234456', $this->shipmentItem->return_key);
		$this->assertEquals('A', $this->shipmentItem->return_letter);
		$this->assertEquals(true, $this->shipmentItem->deliverable);
	}

	public function testGetJson ()
	{
		$this->shipmentItem->properties = array(
			'order_item_id' => 1234,
			'product_variant_id' => 'variant',
			'return_key' => '1234456',
			'return_letter' => 'A',
			'deliverable' => true,
		);

		$expectedJson = '{"order_item_id":1234,"product_variant_id":"variant","return_key":"1234456","return_letter":"A","deliverable":true}';

		$this->assertEquals($expectedJson, $this->shipmentItem->toJson());
	}
}
