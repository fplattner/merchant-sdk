<?php

class AddressShippingTest extends PHPUnit_Framework_TestCase
{
	/* @var \mapi\AddressShipping $addressShipping */
	private $addressShipping;

	public function setUp ()
	{
		$this->addressShipping = new \mapi\AddressShipping();
	}

	public function tearDown ()
	{
		$this->addressShipping = null;
	}

	public function testMassAssignment ()
	{
		$this->addressShipping->properties = array(
			'company'      => '',
			'firstname'    => 'Tim',
			'lastname'     => 'Tester',
			'salutation'   => 'm',
			'street'       => 'My Street',
			'number'       => '13b',
			'additional'   => '',
			'zip_code'     => '42345',
			'city'         => 'MyCity',
			'country_code' => 'DEU',
			'phone'        => '65874359745',
			'department'   => '',
		);

		$this->assertEquals('', $this->addressShipping->company);
		$this->assertEquals('Tim', $this->addressShipping->firstname);
		$this->assertEquals('Tester', $this->addressShipping->lastname);
		$this->assertEquals('m', $this->addressShipping->salutation);
		$this->assertEquals('My Street', $this->addressShipping->street);
		$this->assertEquals('13b', $this->addressShipping->number);
		$this->assertEquals('', $this->addressShipping->additional);
		$this->assertEquals('42345', $this->addressShipping->zip_code);
		$this->assertEquals('MyCity', $this->addressShipping->city);
		$this->assertEquals('DEU', $this->addressShipping->country_code);
		$this->assertEquals('65874359745', $this->addressShipping->phone);
		$this->assertEquals('', $this->addressShipping->department);
	}

	public function testSetGetCompany ()
	{
		$this->addressShipping->setCompany('my corp');
		$this->assertEquals('my corp', $this->addressShipping->getCompany());
	}

	public function testSetGetFirstname ()
	{
		$this->addressShipping->setFirstname('Tim');
		$this->assertEquals('Tim', $this->addressShipping->getFirstname());
	}

	public function testSetGetLastname ()
	{
		$this->addressShipping->setLastname('Tester');
		$this->assertEquals('Tester', $this->addressShipping->getLastname());
	}

	public function testSetGetSalutation ()
	{
		$this->addressShipping->setSalutation('m');
		$this->assertEquals('m', $this->addressShipping->getSalutation());
	}

	public function testSetGetStreet ()
	{
		$this->addressShipping->setStreet('my street');
		$this->assertEquals('my street', $this->addressShipping->getStreet());
	}

	public function testSetGetNumber ()
	{
		$this->addressShipping->setNumber('123');
		$this->assertEquals('123', $this->addressShipping->getNumber());
	}

	public function testSetGetAdditional ()
	{
		$this->addressShipping->setAdditional('my corp');
		$this->assertEquals('my corp', $this->addressShipping->getAdditional());
	}

	public function testSetGetZipCode ()
	{
		$this->addressShipping->setZipCode('45687');
		$this->assertEquals('45687', $this->addressShipping->getZipCode());
	}

	public function testSetGetCity ()
	{
		$this->addressShipping->setCity('my city');
		$this->assertEquals('my city', $this->addressShipping->getCity());
	}

	public function testSetGetCountryCode ()
	{
		$this->addressShipping->setCountryCode('DEU');
		$this->assertEquals('DEU', $this->addressShipping->getCountryCode());
	}

	public function testSetGetPhone ()
	{
		$this->addressShipping->setPhone('45674213');
		$this->assertEquals('45674213', $this->addressShipping->getPhone());
	}

	public function testSetGetDepartment ()
	{
		$this->addressShipping->setDepartment('my dept');
		$this->assertEquals('my dept', $this->addressShipping->getDepartment());
	}

	public function testPropertyAccessCompany ()
	{
		$this->addressShipping->company = 'my corp';
		$this->assertEquals('my corp', $this->addressShipping->company);
	}

	public function testPropertyAccessFirstname ()
	{
		$this->addressShipping->firstname ='Tim';
		$this->assertEquals('Tim', $this->addressShipping->firstname);
	}

	public function testPropertyAccessLastname ()
	{
		$this->addressShipping->lastname = 'Tester';
		$this->assertEquals('Tester', $this->addressShipping->lastname);
	}

	public function testPropertyAccessSalutation ()
	{
		$this->addressShipping->salutation = 'm';
		$this->assertEquals('m', $this->addressShipping->salutation);
	}

	public function testPropertyAccessStreet ()
	{
		$this->addressShipping->street = 'my street';
		$this->assertEquals('my street', $this->addressShipping->street);
	}

	public function testPropertyAccessNumber ()
	{
		$this->addressShipping->number = '123';
		$this->assertEquals('123', $this->addressShipping->number);
	}

	public function testPropertyAccessAdditional ()
	{
		$this->addressShipping->additional = 'my corp';
		$this->assertEquals('my corp', $this->addressShipping->additional);
	}

	public function testPropertyAccessZipCode ()
	{
		$this->addressShipping->zip_code = '45687';
		$this->assertEquals('45687', $this->addressShipping->zip_code);
	}

	public function testPropertyAccessCity ()
	{
		$this->addressShipping->city = 'my city';
		$this->assertEquals('my city', $this->addressShipping->city);
	}

	public function testPropertyAccessCountryCode ()
	{
		$this->addressShipping->country_code = 'DEU';
		$this->assertEquals('DEU', $this->addressShipping->country_code);
	}

	public function testPropertyAccessPhone ()
	{
		$this->addressShipping->phone = '6587435';
		$this->assertEquals('6587435', $this->addressShipping->phone);
	}

	public function testPropertyAccessDepartment ()
	{
		$this->addressShipping->department = 'my dept';
		$this->assertEquals('my dept', $this->addressShipping->department);
	}

	public function testGetJson ()
	{
		$this->addressShipping->properties = array(
			'company'      => '',
			'firstname'    => 'Tim',
			'lastname'     => 'Tester',
			'salutation'   => 'm',
			'street'       => 'My Street',
			'number'       => '13b',
			'additional'   => '',
			'zip_code'     => '42345',
			'city'         => 'MyCity',
			'country_code' => 'DEU',
			'phone'        => '65874359745',
			'department'   => '',
		);

		$expectedJson = '{"company":"","firstname":"Tim","lastname":"Tester","salutation":"m","street":"My Street","number":"13b","additional":"","zip_code":"42345","city":"MyCity","country_code":"DEU","phone":"65874359745","department":""}';

		$this->assertEquals($expectedJson, $this->addressShipping->toJson());
	}

	/**
	 * @expectedException InvalidArgumentException
	 */
	public function testSetCountryCodeValidation ()
	{
		$this->addressShipping->setCountryCode('jfidsfa');
	}
}
