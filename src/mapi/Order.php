<?php

namespace mapi;

use mapi\base\CrudItem;
use mapi\base\Item;


/**
 * Class Order
 *
 * @property \string                $id
 * @property \int                   $customer_id
 * @property \mapi\AddressShipping  $address_shipping
 * @property \mapi\OrderItem[]      $items
 *
 * @method \mapi\Order           setId()               setId(\int $id)                                set the order_id
 * @method \mapi\Order           setCustomerId()       setCustomerId(\int $id)                        set the customer_id
 * @method \mapi\Order           setAddressShipping()  setAddressShipping(\mapi\AddressShipping $as)  set the address
 * @method \mapi\Order           setItems()            setItems(array $items)                         set the items
 * @method \int                  getCustomerId()       getCustomerId()                                get the customer_id
 * @method \mapi\AddressShipping getAddressShipping()  getAddressShipping()                           get the address
 * @method \mapi\OrderItem[]     getItems()            getItems()                                     get the items
 *
 * @package mapi
 */
class Order extends Item
{
	protected $_properties = array(
		'id'               => array ('string', null),
		'customer_id'      => array ('int', null),
		'address_shipping' => array ('\\mapi\\AddressShipping', null),
		'items'            => array ('array', array()),
	);


	public function addItem (OrderItem $item)
	{
		$this->_properties['items'][$this->valueIndex][] = $item;
		return $this;
	}


	protected function assignItems (array $data)
	{
		if (count($data) > 0 && is_array($data[0]))
		{
			foreach ($data as $content)
			{
				$this->addItem(new OrderItem($content));
			}
		}
		elseif (count($data) > 0 && $data[0] instanceof Item)
		{
			foreach ($data as $content)
			{
				$this->addItem($content);
			}
		}
		else
		{
			$this->addItem(new OrderItem($data));
		}
	}
}
