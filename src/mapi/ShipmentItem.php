<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class ShipmentItem
 *
 * @property \mapi\OrderItem      order_item
 * @property \mapi\ProductVariant product_variant
 * @property \string              return_key
 * @property \string              return_letter
 * @property \bool                deliverable
 *
 * @method \mapi\ShipmentItem   setOrderItem()      setOrderItem(\mapi\OrderItem $item)              set OrderItem
 * @method \mapi\ShipmentItem   setProductVariant() setProductVariant(\mapi\ProductVariant $variant) set ProductVariant
 * @method \mapi\ShipmentItem   setReturnKey()      setReturnKey(\string $key)                       set return_key
 * @method \mapi\ShipmentItem   setReturnLetter()   setReturnLetter(\string $letter)                 set return_letter
 * @method \mapi\ShipmentItem   setDeliverable()    setDeliverable(\bool $deliverable)               set deliverable
 * @method \mapi\OrderItem      getOrderItem()      getOrderItem()                                   get OrderItem
 * @method \mapi\ProductVariant getProductVariant() getProductVariant()                              get ProductVariant
 * @method \string              getReturnKey()      getReturnKey()                                   get return_key
 * @method \string              getReturnLetter()   getReturnLetter()                                get return_letter
 * @method \bool                getDeliverable()    getDeliverable()                                 get deliverable
 *
 * @package mapi
 */
class ShipmentItem extends Item
{
	protected $_properties = array(
		'order_item'         => array ('\\mapi\\OrderItem', null),
		'product_variant'    => array ('\\mapi\\ProductVariant', null),
		'return_key'         => array ('string', null),
		'return_letter'      => array ('string', null),
		'deliverable'        => array ('bool', null),
	);


	protected function assignProductVariantId ($value)
	{
		$variant = new ProductVariant();
		$variant->id = $value;
		$this->product_variant = $variant;
	}


	protected function assignOrderItemId ($value)
	{
		$orderItem = new OrderItem();
		$orderItem->id = $value;
		$this->order_item = $orderItem;
	}


	public function getKeyValuePairs ($includeId = true)
	{
		return array(
			"order_item_id" => $this->order_item->id,
			"product_variant_id" => $this->product_variant->id,
			"return_key" => $this->return_key,
			"return_letter" => $this->return_letter,
			"deliverable" => $this->deliverable,
		);
	}
}
