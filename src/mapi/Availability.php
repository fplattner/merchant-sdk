<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class Availability
 *
 * @property \mapi\ProductVariant $product_variant
 * @property \int                 $quantity
 *
 * @method Availability   setProductVariant()  setProductVariant(ProductVariant $variant)  Set the ProductVariant of which to get the availability.
 * @method ProductVariant getProductVariant()  getProductVariant()                         Get the ProductVariant if which ...
 * @method Availability   setQuantity()	       setQuantity(\int $quantity)                 Set the available quantity
 * @method \int           getQuantity()	       getQuantity()                               Get the available quantity
 *
 * @package mapi
 */
class Availability extends Item
{
	protected $_properties = array (
		'product_variant' => array ('\\mapi\\ProductVariant', null),
		'quantity'        => array ('int', null),
	);

	public function getKeyValuePairs ($includeId = true)
	{
		$retVal = array(
			'quantity' => $this->getQuantity(),
		);

		if ($includeId)
		{
			$retVal['id'] = $this->getProductVariant()->getId();
		}

		return $retVal;
	}

	protected function assignId ($data)
	{
		$variant = new ProductVariant();
		$variant->id = $data;
		$this->product_variant = $variant;
	}
}
