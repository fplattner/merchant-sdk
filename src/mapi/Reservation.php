<?php

namespace mapi;

use mapi\base\Item;

/**
 * Class Reservation
 *
 * @property string               $id
 * @property \mapi\ProductVariant $product_variant
 * @property int                  $quantity
 *
 * @method Reservation          setId()               setId(\string $id)                               Set the Reservation ID.
 * @method Reservation          setProductVariant()   setProductVariant(\mapi\ProductVariant $variant) Set the ProductVariant.
 * @method Reservation          setQuantity()         setQuantity(\int $quantity)                      Set the reserved quantity.
 * @method \string              getId()               getId()                                          Get the Reservation ID.
 * @method \mapi\ProductVariant getProductVariant()   getProductVariant()                              Get the ProductVariant.
 * @method \int                 getQuantity()         getQuantity()                                    Get the reserved quantity.
 *
 * @package mapi
 */
class Reservation extends Item
{
	protected $_properties = array (
		'id'              => array ('string', null),
		'product_variant' => array ('\\mapi\\ProductVariant', null),
		'quantity'        => array ('int', null),
	);


	public function getKeyValuePairs ($includeId = true)
	{
		$retVal = array (
			'product_variant_id' => $this->product_variant->id,
			'quantity' => $this->quantity,
		);

		if ($includeId)
		{
			$retVal['reservation_id'] = $this->id;
		}

		return $retVal;
	}

	protected function assignProductVariantId ($data)
	{
		$variant = new ProductVariant();
		$variant->id = $data;
		$this->product_variant = $variant;
	}

	protected function assignReservationId ($value)
	{
		$this->id = $value;
	}
}
