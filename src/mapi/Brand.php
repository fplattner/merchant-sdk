<?php

namespace mapi;
use mapi\base\Item;

/**
 * Class Brand
 *
 * @property string $name
 * @property int    $id
 *
 * @method \mapi\Brand setName() setName(\string $name) set the name
 * @method \mapi\Brand setId()   setid(\int $id)        set the id
 * @method \string     getName() getName()              get the name
 * @method \int        getId()   getId()                get the id
 *
 * @package mapi
 */
class Brand extends Item
{
	protected $_properties = array(
		'name' => array ('string', null),
		'id'   => array ('int', null),
	);


	public static function loadAll ()
	{
		list($status, $response) = static::$connector->request('GET', 'brands');

		$response = @json_decode($response, true);

		if ($status == 200 && is_array($response))
		{
			$retVal = array();
			foreach($response as $values)
			{
				$attribute = new Brand($values);
				$retVal[] = $attribute;
			}

			return $retVal;
		}

		return intval($status, 10);
	}
}
