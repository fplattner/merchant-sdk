<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class Product
 *
 * @property string                 $id
 * @property string                 $name
 * @property string                 $description_long
 * @property string                 $description_short
 * @property \mapi\Brand            $brand
 * @property bool                   $active
 * @property \mapi\ProductVariant[] $variants
 * @property \mapi\Category[]       $categories
 * @property string[]               $images
 *
 * @method string                 getId()               getId()                          get the id
 * @method string                 getName()             getName()                        get the name
 * @method string                 getDescriptionLong()  getDescriptionLong()             get the description
 * @method string                 getDescriptionShort() getDescriptionShort()            get the description
 * @method \mapi\Brand            getBrand()            getBrand()                       get the brand
 * @method bool                   getActive()           getActive()                      get active state
 * @method \mapi\ProductVariant[] getVariants()         getVariants()                    get the variants
 * @method \mapi\Category[]       getCategories()       getCategories()                  get the categories
 * @method string[]               getImages()           getImages()                      get the images
 * @method \mapi\Product          setId()               setId(\string $id)               set the id
 * @method \mapi\Product          setBrand()            setBrand(\mapi\Brand $brand)     set the brand
 * @method \mapi\Product          setActive()           setActive(\bool $active)         set active state
 * @method \mapi\Product          setVariants()         setVariants(array $variants)     set the variants
 * @method \mapi\Product          setCategories()       setCategories(array $categories) set the categories
 * @method \mapi\Product          setImages()           setImages(array $images)         set the images
 *
 * @package mapi
 */
class Product extends Item
{
	protected $_properties = array(
		'id'                => array ('string', null),
		'name'              => array ('string', null),
		'description_long'  => array ('string', null),
		'description_short' => array ('string', null),
		'brand'             => array ('\\mapi\\Brand', null),
		'active'            => array ('bool', null),
		'variants'          => array ('array', array()),
		'categories'        => array ('array', array()),
		'images'            => array ('array', array()),
	);


	public function assignImages (array $images)
	{
		$tmpImgs = array();
		foreach ($images as $image)
		{
			if (filter_var($image, FILTER_VALIDATE_URL) === $image)
			{
				$tmpImgs[] = $image;
			}
			else
			{
				throw new \InvalidArgumentException('images must be an array of urls');
			}
		}
		$this->_properties['images'][$this->valueIndex] = $tmpImgs;
		return $this;
	}


	public function addImage ($image)
	{
		trigger_error("Images on a Product may lead to undesirable image display. It is strongly recommended to only use images for ProductVariants; not for Products.", E_WARNING);
		if (filter_var($image, FILTER_VALIDATE_URL) === $image)
		{
			$this->_properties['images'][] = $image;
		}
		else
		{
			throw new \InvalidArgumentException($image . ' is not a valid url.');
		}

		return $this;
	}


	protected function assignCategories (array $categories)
	{
		$tmpCats = array();
		foreach ($categories as $id)
		{
			if (is_int($id))
			{
				$category = new Category();
				$category->id = $id;
				$tmpCats[] = $category;

			}
			elseif ($id instanceof Category)
			{
				$tmpCats[] = $id;
			}
			else
			{
				throw new \InvalidArgumentException('categories must either be of type array or \\mapi\\Category');
			}
		}
		$this->_properties['categories'][$this->valueIndex] = $tmpCats;

		return $this;
	}

	public function addCategory (Category $category)
	{
		$this->_properties['categories'][] = $category;
		return $this;
	}


	protected function assignVariants (array $variants)
	{
		$tmpCats = array();
		foreach ($variants as $variant)
		{
			if (is_array($variant))
			{
				$variant = new ProductVariant($variant);
				$tmpCats[] = $variant;
			}
			elseif ($variant instanceof ProductVariant)
			{
				$variant->setParent($this);
				$tmpCats[] = $variant;
			}
			else
			{
				throw new \InvalidArgumentException('variants must either be of type array or \\mapi\\ProductVariant');
			}
		}
		$this->_properties['variants'][$this->valueIndex] = $tmpCats;

		return $this;
	}

	public function addVariant (ProductVariant $variant)
	{
		$variant->setParent($this);
		$this->_properties['variants'][$this->valueIndex][] = $variant;
		return $this;
	}

	public function setDescriptionLong ($desc)
	{
		if (mb_strlen($desc) > 65535)
		{
			throw new \InvalidArgumentException('description_long can be at most 65535 chars long.');
		}

		$this->_properties['description_long'][$this->valueIndex] = $desc;
		return $this;
	}

	public function setDescriptionShort ($desc)
	{
		if (mb_strlen($desc) > 65535)
		{
			throw new \InvalidArgumentException('description_short can be at most 65535 chars long.');
		}

		$this->_properties['description_short'][$this->valueIndex] = $desc;
		return $this;
	}

	public function setName ($name)
	{
		if (mb_strlen($name) > 255)
		{
			throw new \InvalidArgumentException('name can be at most 255 chars long.');
		}
		$this->_properties['name'][$this->valueIndex] = $name;
		return $this;
	}

	protected function assignBrandId ($data)
	{
		$brand = new Brand();
		$brand->id = $data;
		$this->_properties['brand'][$this->valueIndex] = $brand;
	}

	public function getKeyValuePairs($includeId = true)
	{
		$retVal = array(
			'name' => $this->name,
			'description_short' => $this->description_short,
			'description_long' => $this->description_long,
			'brand_id' => $this->brand->id,
			'active' => $this->active,
			'images' => $this->images,
		);

		$retVal['variants'] = array();
		foreach ($this->variants as $variant)
		{
			$retVal['variants'][] = $variant->getKeyValuePairs();
		}

		$retVal['categories'] = array();
		foreach ($this->categories as $category)
		{
			$retVal['categories'][] = $category->id;
		}

		if ($includeId)
		{
			$retVal['id'] = $this->id;
		}

		return $retVal;
	}
}
