<?php

namespace mapi\base;

class CurlConnector extends Connector
{
	public function request ($method, $endPoint, $body = null)
	{
		$curl = curl_init($this->getMapiBaseUrl() . $endPoint);

		curl_setopt_array($curl, array(
			CURLOPT_HTTPHEADER => $this->getHeaders($body == null ? 0 : strlen($body)),
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_RETURNTRANSFER => true,
		));

		switch ($method)
		{
			case 'GET':
				break;
			case 'POST':
				curl_setopt_array($curl, array(
					CURLOPT_POST => true,
					CURLOPT_CUSTOMREQUEST => 'POST',
					CURLOPT_POSTFIELDS => $body,
				));
				break;

			case 'PUT':
				curl_setopt_array($curl, array(
					CURLOPT_CUSTOMREQUEST => 'PUT',
					CURLOPT_POSTFIELDS => $body,
				));
				break;

			case 'DELETE':
				curl_setopt_array($curl, array(
					CURLOPT_CUSTOMREQUEST => 'DELETE',
				));
				break;
			default:
				throw new \InvalidArgumentException('Method: ' . $method . ' not supported.');
		}

		$response = curl_exec($curl);

		if (curl_errno($curl) !== 0)
		{
			throw new \Exception('CURL message: ' . curl_error($curl));
		}

		$httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		return array($httpStatus, $response);
	}
}
