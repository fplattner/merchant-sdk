<?php

namespace mapi\base;


/**
 * Contains the property logic
 *
 * @property array $properties
 *
 * @package mapi\base
 */
abstract class Item
{
	/* @var \mapi\base\Connector $connector */
	protected static $connector;


	/**
	 * properties, that this model can store. format is: array('attributeName' => array('datatype', [currently assigned value])).
	 *
	 * @var array
	 */
	protected $_properties = array (
		'id' => array ('int', null),
	);

	/**
	 * at which position in one $properties entry can the currently assigned value be found.
	 *
	 * @var int
	 */
	protected $valueIndex = 1;

	/**
	 * at which position in one $properties entry can the current iterator position be found. Only applicable to type array.
	 *
	 * @var int
	 */
	protected $iteratorIndex = 2;

	/**
	 * at which position in one $properties entry can the type information be found.
	 *
	 * @var int
	 */
	protected $typeIndex = 0;


	public function __construct (array $properties = null, Connector $connector = null)
	{
		if ($properties !== null)
		{
			// This is NOT a direct assignment to $this->_properties, but to activate mass assignment.
			// So the missing underscore is intentional.
			$this->properties = $properties;
		}

		if (null === $connector)
		{
			static::$connector = new CurlConnector();
		}
		else
		{
			static::$connector = $connector;
		}
	}


	public function __set ($name, $value)
	{
		if ($name == 'properties')
		{
			$this->massAssign($value);
		}
		else
		{
			$this->{'set' . $this->camelCase($name)}($value);
		}
	}


	public function __get ($name)
	{
		return $this->{'get' . $this->camelCase($name)}();
	}


	public function __isset ($name)
	{
		return (isset($this->_properties[$name]) && count($this->_properties[$name]) > $this->valueIndex)
			&& (($this->_properties[$name][$this->typeIndex] !== 'array' && !empty($this->_properties[$name][$this->valueIndex]))
					|| count($this->_properties[$name][$this->valueIndex]) > 0);
	}


	public function __call ($name, $value)
	{
		$simpleTypes = array('int', 'float', 'bool', 'string');
		$action = substr($name, 0, 3);
		$property = strtolower(substr($name, 3));
		if ('set' == $action)
		{
			$value = $value[0]; // only one argument can get passed
			if (method_exists($this, 'assign' . ucfirst($property)))
			{
				$this->{'assign' . $this->camelCase($property)}($value);
				return $this;
			}
			elseif (in_array($this->_properties[$property][$this->typeIndex], $simpleTypes))
			{
				$testFunction = 'is_' . $this->_properties[$property][$this->typeIndex];
				if (!$testFunction($value) && !is_null($value))
				{
					throw new \InvalidArgumentException('value must be of type ' . $this->_properties[$property][$this->typeIndex] . ' for property ' . $property);
				}
				$this->_properties[$property][$this->valueIndex] = $value;
				return $this;
			}
			elseif (is_array($value))
			{
				if ('array' == $this->_properties[$property][$this->typeIndex])
				{
					$this->_properties[$property][$this->valueIndex] = $value;
				}
				elseif (is_subclass_of($this->_properties[$property][$this->typeIndex], '\\mapi\\base\\Item'))
				{
					$className = $this->_properties[$property][$this->typeIndex];
					$tmpItem = new $className($value);
					$this->_properties[$property][$this->valueIndex] = $tmpItem;
				}
				return $this;
			}
			elseif (is_a($value, $this->_properties[$property][$this->typeIndex]))
			{
				$this->_properties[$property][$this->valueIndex] = $value;
				return $this;
			}
		}
		elseif ('get' == $action && isset($this->_properties[$property]))
		{
			return $this->_properties[$property][$this->valueIndex];
		}

		throw new \InvalidArgumentException('method ' . $name . ' does not exist');
	}

	/**
	 * creates a json string out of this model and it's items
	 *
	 * @param $includeId boolean whether to include model ids. During update the id is encoded in the url.
	 *
	 * @return mixed
	 */
	public function toJson ($includeId = true)
	{
		$properties = $this->getKeyValuePairs($includeId);
		return json_encode($properties);
	}

	/**
	 * returns all properties as an array in the form: array('attributeName' => 'attributeValue')
	 *
	 * @param bool $includeId
	 *
	 * @return array
	 */
	public function getKeyValuePairs ($includeId = true)
	{
		$keyVal = array ();
		$simpleTypes = array('int', 'float', 'bool', 'string');
		foreach ($this->_properties as $name => $value)
		{
			if (!empty($value) && ($includeId || $name != 'id')) // exclude all properties that are empty
			{
				$type = $value[$this->typeIndex];
				$value = $value[$this->valueIndex];

				if (in_array($type, $simpleTypes)) // transfer simple types directly
				{
					$keyVal[$name] = $value;
				}
				elseif ('array' == $type) // transfer arrays element by element
				{
					$keyVal[$name] = array();
					foreach ($value as $val)
					{
						if ($val instanceof Item)
						{
							$keyVal[$name][] = $val->getKeyValuePairs(true);
						}
						else
						{
							$keyVal[$name][] = $val;
						}
					}
				}
				elseif ($value instanceof Item) // transfer Items recursively
				{
					$keyVal[$name] = $value->getKeyValuePairs(true);
				}
			}
		}

		return $keyVal;
	}

	public function __toString ()
	{
		return $this->toJson();
	}

	protected function massAssign (array $data)
	{
		foreach ($data as $name => $value)
		{
			$this->$name = $value;
		}
	}

	public function setMapiBaseUrl ($url)
	{
		static::$connector->setMapiBaseUrl($url);
		return $this;
	}

	public function setUsername ($username)
	{
		static::$connector->setUsername($username);
		return $this;
	}

	public function setPassword ($password)
	{
		static::$connector->setPassword($password);
		return $this;
	}

	protected function camelCase ($string)
	{
		// more than 5 times faster than regex
		return implode('', array_map("ucfirst", explode('_', $string)));
	}
}
