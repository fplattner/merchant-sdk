<?php

namespace mapi\base;

abstract class Connector
{
	/**
	 * the mapi base url
	 *
	 * @var string
	 */
	private $mapiBaseUrl;

	/**
	 * used to login to MAPI endpoint
	 *
	 * @var string
	 */
	private $username;

	/**
	 * used to login to MAPI endpoint
	 *
	 * @var string
	 */
	private $password;

	/**
	 * @param string $mapiBaseUrl
	 */
	public function setMapiBaseUrl ($mapiBaseUrl)
	{
		$this->mapiBaseUrl = $mapiBaseUrl;
	}

	/**
	 * @return string
	 */
	public function getMapiBaseUrl ()
	{
		return $this->mapiBaseUrl;
	}

	/**
	 * @param string $password
	 */
	public function setPassword ($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getPassword ()
	{
		return $this->password;
	}

	/**
	 * @param string $username
	 */
	public function setUsername ($username)
	{
		$this->username = $username;
	}

	/**
	 * @return string
	 */
	public function getUsername ()
	{
		return $this->username;
	}


	protected function getHeaders ($bodyLength = 0)
	{
		$retVal = array (
			'Authorization: Basic ' . base64_encode($this->username . ':' . $this->password),
		);

		if ($bodyLength > 0)
		{
			$retVal[] = 'Content-type: application/json';
			$retVal[] = 'Content-length: ' . $bodyLength;
		}

		return $retVal;
	}


	/**
	 * Sends an HTTP Request and returns an array containing the HTTP Status-Code and the raw response body.
	 *
	 * @param string      $method
	 * @param             $endpoint
	 * @param null|string $body
	 *
	 * @return array in the form: array(200, 'MAPI-RESPONSE-BODY')
	 */
	public abstract function request ($method, $endpoint, $body = null);
}