<?php

namespace mapi;

use mapi\base\Item;

/**
 * Class OrderItem
 *
 * @property int                  $id
 * @property \mapi\ProductVariant $product_variant
 * @property \int                 $collins_product_variant_id
 * @property \int                 $quantity
 * @property Reservation          $reservation
 *
 * @method \mapi\OrderItem      setId()                         setId(\int $id)                              set the id
 * @method \mapi\OrderItem      setProductVariant()             setProductVariant(\mapi\ProductVariant $id)  set the variant
 * @method \mapi\OrderItem      setCollinsProductVariantId()    setCollinsProductVariantId(\int $id)         set the id
 * @method \mapi\OrderItem      setQuantity()                   setQuantity(\int $id)                        set the quantity
 * @method \mapi\OrderItem      setReservation()                setReservation(\mapi\Reservation $res)       set the reservation
 * @method \int                 getId()                         getId()                                      get the id
 * @method \mapi\ProductVariant getProductVariant()             getProductVariant()                          get the variant
 * @method \string              getCollinsProductVariantId()    getCollinsProductVariantId()                 get the id
 * @method \int                 getQuantity()                   getQuantity()                                get the quantity
 * @method \mapi\Reservation    getReservation()                getReservation()                             get the reservation
 *
 * @package mapi
 */
class OrderItem extends Item
{
	protected $_properties = array (
		'id'                         => array ('int', null),
		'product_variant'            => array ('\\mapi\\ProductVariant', null),
		'quantity'                   => array ('int', null),
		'reservation'                => array ('\\mapi\\Reservation', null),
		'collins_product_variant_id' => array ('string', null),
	);

	protected function assignOrderItemId ($value)
	{
		$this->id = $value;
	}

	protected function assignReservationId ($data)
	{
		$reservation = new Reservation();
		$reservation->id = $data;
		$this->reservation = $reservation;
	}

	protected function assignProductVariantId ($data)
	{
		$productVariant = new ProductVariant();
		$productVariant->id = $data;
		$this->product_variant = $productVariant;
	}


	public function getKeyValuePairs ($includeId = true)
	{
		$retVal = array(
			'reservation_id' => $this->reservation->id,
			'merchant_product_variant_id' => $this->product_variant->id,
			'quantity' => $this->quantity,
			'collins_product_variant_id' => $this->collins_product_variant_id,
		);

		if ($includeId)
		{
			$retVal['order_item_id'] = $this->id;
		}

		return $retVal;
	}
}
