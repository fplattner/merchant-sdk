<?php

namespace mapi;

use mapi\base\Item;

/**
 * Class AddressShipping
 *
 * @property string $company
 * @property string $firstname
 * @property string $lastname
 * @property string $salutation
 * @property string $street
 * @property string $number
 * @property string $additional
 * @property string $zip_code
 * @property string $city
 * @property string $country_code
 * @property string $phone
 * @property string $department
 *
 * @method \mapi\AddressShipping setCompany()      setCompany()      set the company
 * @method \mapi\AddressShipping setFirstname()    setFirstname()    set the firstname
 * @method \mapi\AddressShipping setLastname()     setLastname()     set the lastname
 * @method \mapi\AddressShipping setSalutation()   setSalutation()   set the salutation
 * @method \mapi\AddressShipping setStreet()       setStreet()       set the street
 * @method \mapi\AddressShipping setNumber()       setNumber()       set the number
 * @method \mapi\AddressShipping setAdditional()   setAdditional()   set the additional
 * @method \mapi\AddressShipping setZipCode()      setZipCode()      set the company
 * @method \mapi\AddressShipping setCity()         setCity()         set the city
 * @method \mapi\AddressShipping setPhone()        setPhone()        set the phone
 * @method \mapi\AddressShipping setDepartment()   setDepartment()   set the department
 * @method \string               getCompany()      getCompany()      get the company
 * @method \string               getFirstname()    getFirstname()    get the firstname
 * @method \string               getLastname()     getLastname()     get the lastname
 * @method \string               getSalutation()   getSalutation()   get the salutation
 * @method \string               getStreet()       getStreet()       get the street
 * @method \string               getNumber()       getNumber()       get the number
 * @method \string               getAdditional()   getAdditional()   get the additional
 * @method \string               getZipCode()      getZipCode()      get the company
 * @method \string               getCity()         getCity()         get the city
 * @method \string               getPhone()        getPhone()        get the phone
 * @method \string               getDepartment()   getDepartment()   get the department
 * @method \string               getCountryCode()  getCountryCode()  get the country code
 * 
 * @package mapi
 */
class AddressShipping extends Item
{
	protected $_properties = array(
		'company'      => array ('string', null),
		'firstname'    => array ('string', null),
		'lastname'     => array ('string', null),
		'salutation'   => array ('string', null),
		'street'       => array ('string', null),
		'number'       => array ('string', null),
		'additional'   => array ('string', null),
		'zip_code'     => array ('string', null),
		'city'         => array ('string', null),
		'country_code' => array ('string', null),
		'phone'        => array ('string', null),
		'department'   => array ('string', null),
	);


	public function setCountryCode ($countryCode)
	{
		if (strlen($countryCode) !== 3)
		{
			throw new \InvalidArgumentException('countryCode must be a 3 character string (ISO-3166alpha3 country code).');
		}
		$this->_properties['country_code'][$this->valueIndex] = $countryCode;
	}
}
