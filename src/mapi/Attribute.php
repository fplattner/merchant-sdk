<?php

namespace mapi;

use mapi\base\Item;

/**
 * Class Attribute
 *
 * @property string     name
 * @property \string[]  values
 *
 * @method \mapi\Attribute setName()            setName(\string $name)            set the name
 * @method \mapi\Attribute setValues()          setValues(array $values)          set the values
 * @method \string         getName()            getName()                         get the name
 * @method \string[]       getValues()          getValues()                       get the values
 *
 * @package mapi
 */
class Attribute extends Item
{
	protected $_properties = array(
		'name'    	      => array ('string', null),
		'values'  	      => array ('array', array()),
	);

	public static function loadAll ()
	{
		list($status, $response) = static::$connector->request('GET', 'attributes');

		$response = @json_decode($response, true);

		if ($status == 200 && is_array($response))
		{
			$retVal = array();
			foreach($response as $name => $values)
			{
				/* @var \mapi\Attribute $attribute */
				$attribute = new Attribute(compact('name', 'values'));
				$retVal[] = $attribute;
			}

			return $retVal;
		}

		return intval($status, 10);
	}

	public function getKeyValuePairs ($includeId = true)
	{
		return array(
			$this->name => $this->values,
		);
	}
}
