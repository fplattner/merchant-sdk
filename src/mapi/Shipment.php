<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class Shipment
 *
 * @property \string              $id
 * @property \mapi\Order          $order
 * @property \string              $shipment_key
 * @property \string              $carrier
 * @property \string              $delivery_date
 * @property \string              $return_document_url
 * @property \mapi\ShipmentItem[] $items
 *
 * @package mapi
 */
class Shipment extends Item
{
	public $_properties = array(
		'id'                  => array ('string', null),
		'order'               => array ('\\mapi\\Order', null),
		'shipment_key'        => array ('string', null),
		'carrier'             => array ('string', null),
		'delivery_date'       => array ('string', null),
		'return_document_url' => array ('string', null),
		'items'               => array ('array', array()),
	);


	public function __isset ($name)
	{
		if ($name == 'items')
		{
			return count($this->_properties['items'][$this->valueIndex]) > 0;
		}
		else
		{
			return parent::__isset($name);
		}
	}


	public function setDeliveryDate ($date)
	{
		$this->_properties['delivery_date'][$this->valueIndex] = strftime('%Y-%m-%dT%H:%M:%S', strtotime($date));
		return $this;
	}


	protected function assignItems (array $items)
	{
		$tmpItems = array();
		foreach ($items as $item)
		{
			if (is_array($item))
			{
				$tmpItems[] = new ShipmentItem($item);
			}
			elseif ($item instanceof ShipmentItem)
			{
				$tmpItems[] = $item;
			}
			else
			{
				throw new \InvalidArgumentException('all items must be either of type array or \\mapi\\ShipmentItem');
			}
		}
		$this->_properties['items'][$this->valueIndex] = $tmpItems;
		return $this;
	}

	public function addItem (ShipmentItem $item)
	{
		$this->_properties['items'][$this->valueIndex] = $item;
		return $this;
	}

	protected function assignOrderId ($data)
	{
		$order = new Order();
		$order->id = $data;
		$this->order = $order;
	}

	public function getKeyValuePairs($includeId)
	{
		$retVal = array(
			'shipment_key' => $this->shipment_key,
			'carrier' => $this->carrier,
			'delivery_date' => $this->delivery_date,
			'return_document_url' => $this->return_document_url,
		);

		$retVal['items'] = array();
		/* @var \mapi\ShipmentItem $item */
		foreach ($this->items as $item)
		{
			$retVal['items'][] = $item->getKeyValuePairs(true);
		}

		if ($includeId)
		{
			$retVal['id'] = $this->id;
			$retVal['order_id'] = $this->order->id;
		}

		return $retVal;
	}
}
