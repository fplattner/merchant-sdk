<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class ProductVariant
 *
 * @property string            $id
 * @property int               $price
 * @property int               $retail_price
 * @property int               $red_price
 * @property int               $purchase_price
 * @property int               $min_purchase
 * @property int               $max_purchase
 * @property string            $currency
 * @property string            $ean
 * @property bool              $default
 * @property bool              $active
 * @property int               $quantity
 * @property string[]          $images
 * @property \mapi\Attribute[] $attributes
 *
 * @package mapi
 */
class ProductVariant extends Item
{
	protected $_properties = array(
		'id'             => array ('string', null),
		'price'          => array ('int', null),
		'retail_price'   => array ('int', null),
		'red_price'      => array ('int', null),
		'purchase_price' => array ('int', null),
		'min_purchase'   => array ('int', null),
		'max_purchase'   => array ('int', null),
		'currency'       => array ('string', 'EUR'),
		'ean'            => array ('string', null),
		'default'        => array ('bool', false),
		'active'         => array ('bool', false),
		'quantity'       => array ('int', 0),
		'images'         => array ('array', array()),
		'attributes'     => array ('array', array()),
	);
	/* @var Product $parent */
	private $parent;
	/* @var string $parentId */
	private $parentId;



	public function assignImages (array $images)
	{
		$tmpImages = array();
		foreach ($images as $image)
		{
			if (filter_var($image, FILTER_VALIDATE_URL) === $image)
			{
				$tmpImages[] = $image;
			}
			else
			{
				throw new \InvalidArgumentException($image . ' is not a valid url.');
			}
		}
		$this->_properties['images'][$this->valueIndex] = $tmpImages;
		return $this;
	}


	public function addImage ($image)
	{
		if (filter_var($image, FILTER_VALIDATE_URL) === $image)
		{
			$this->_properties['images'][$this->valueIndex] = $image;
		}
		else
		{
			throw new \InvalidArgumentException($image . ' is not a valid url.');
		}

		return $this;
	}


	public function setAttributes (array $attributes)
	{
		$tmpAttrs = array();
		foreach ($attributes as $name => $values)
		{
			if (is_array($values))
			{
				$attrs = new Attribute();
				$attrs->name = $name;
				$attrs->values = $values;
				$tmpAttrs[] = $attrs;
			}
			elseif ($values instanceof Attribute)
			{
				$tmpAttrs[] = $values;
			}
			else
			{
				throw new \InvalidArgumentException('attributes must either be of type array or \\mapi\\Attribute');
			}
		}
		$this->_properties['attributes'][$this->valueIndex] = $tmpAttrs;
		return $this;
	}

	public function addAttribute (Attribute $attribute)
	{
		$this->_properties['attributes'][$this->valueIndex] = $attribute;
		return $this;
	}


	protected function methodAllowed ($endpoint, $method)
	{
		return ($method == 'POST' && preg_match('/^product\/.*\/variant$/', $endpoint))
				|| (in_array($method, array('GET', 'PUT', 'DELETE')) && preg_match('/^product\/.+\/variant\/.+$/', $endpoint));
	}


	public function getKeyValuePairs($includeId = true)
	{
		$retVal = array(
			'price' => $this->price,
			'retail_price' => $this->retail_price,
			'red_price' => $this->red_price,
			'purchase_price' => $this->purchase_price,
			'min_purchase' => $this->min_purchase,
			'max_purchase' => $this->max_purchase,
			'currency' => $this->currency,
			'ean' => $this->ean,
			'default' => $this->default,
			'quantity' => $this->quantity,
			'images' => $this->images,
		);

		$retVal['attributes'] = array();

		/* @var \mapi\Attribute $attribute */
		foreach ($this->attributes as $attribute)
		{
			$retVal['attributes'] = array_merge($retVal['attributes'], $attribute->getKeyValuePairs());
		}

		if ($includeId)
		{
			$retVal['id'] = $this->id;
		}

		return $retVal;
	}


	/**
	 * set id and call this to set this variant active. i.e. available for sale.
	 *
	 * @return bool
	 * @throws \InvalidArgumentException
	 */
	public function saveActive ()
	{
		if (empty($this->id))
		{
			throw new \InvalidArgumentException('id cannot be empty for saveActive()');
		}

		$endpoint = 'product/' . $this->getParent()->id . '/variant/' . $this->id . '/active';

		list($status, $data) = static::$connector->request($endpoint, 'PUT');

		if ($status == 200)
		{
			$this->active = true;
			return true;
		}

		return $status;
	}

	/**
	 * set id and call this to set this variant inactive. ie. not available for sale.
	 *
	 * @return bool
	 * @throws \InvalidArgumentException
	 */
	public function saveInactive ()
	{
		if (empty($this->id))
		{
			throw new \InvalidArgumentException('id cannot be empty for saveInactive()');
		}

		$endpoint = 'product/' . $this->getParentId() . '/variant/' . $this->id . '/inactive';

		list($status, $data) = static::$connector->request($endpoint, 'PUT');

		if ($status == 200)
		{
			$this->active = false;
			return true;
		}

		return $status;
	}

	/**
	 * sets the parent model.
	 * $parentId is automatically set to the new parent's id.
	 *
	 * @param mixed $parent
	 *
	 * @return $this
	 */
	public function setParent (Product $parent)
	{
		$this->parent   = $parent;
		$this->parentId = $parent->id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getParent ()
	{
		return $this->parent;
	}

	/**
	 * sets the $parentId
	 * if $parent is set and the $parentId does not match $parent is set to null
	 *
	 * @param mixed $parentId
	 */
	public function setParentId ($parentId)
	{
		$this->parentId = $parentId;

		if ($this->parentId !== $this->parent->id)
		{
			$this->parent = null;
		}
	}

	/**
	 * @return mixed
	 */
	public function getParentId ()
	{
		return $this->parentId;
	}
}
