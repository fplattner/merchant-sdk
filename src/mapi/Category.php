<?php

namespace mapi;

use mapi\base\Item;


/**
 * Class Category
 *
 * @property \int             id
 * @property \int             parent_id
 * @property \string          name
 * @property \mapi\Category[] children
 *
 * @method \mapi\Category   setId()        setId(\int $id)                   set the id
 * @method \mapi\Category   setParent()    setParent(\mapi\Category $parent) set the parent_id
 * @method \mapi\Category   setName()      setName(\string $name)            set the name
 * @method \int             getId()        getId()                           get the id
 * @method \mapi\Category   getParent()    getParent()                       get the parent_id
 * @method \string          getName()      getName()                         get the name
 * @method \mapi\Category[] getChildren()  getChildren()                     get the children ids
 *
 * @package mapi
 */
class Category extends Item
{
	// all categories created during mass assignment are stored here and reused, so memory consumption
	private static $categoryRegistry = array();

	protected $_properties = array(
		'id'       => array ('int', 0),
		'parent'   => array ('\\mapi\\Category', 0),
		'name'     => array ('string', null),
		'children' => array ('array', array ()),
	);


	protected function assignParentId ($data)
	{
		$this->_properties['parent'][$this->valueIndex] = $this->getCategoryFromRegistry($data);
	}


	protected function assignChildren ($children)
	{
		$tmpChildren = array();
		foreach ($children as $category)
		{
			if (!is_int($category))
			{
				throw new \InvalidArgumentException('child categories must be given as an int.');
			}

			$tmpChildren[] = $this->getCategoryFromRegistry($category);
		}
		$this->_properties['children'][$this->valueIndex] = $tmpChildren;

		return $this;
	}


	public static function loadAll ()
	{
		list($status, $response) = static::$connector->request('GET', 'categories');

		$response = @json_decode($response, true);

		if ($status == 200 && is_array($response))
		{
			$retVal = array();
			foreach($response as $values)
			{
				/* @var \mapi\Attribute $attribute */
				$attribute = new Category($values);
				$retVal[] = $attribute;
			}

			return $retVal;
		}

		return intval($status, 10);
	}


	protected function getCategoryFromRegistry ($id)
	{
		if (empty(static::$categoryRegistry[$id]))
		{
			static::$categoryRegistry[$id] = new Category();
			static::$categoryRegistry[$id]->id = $id;
		}

		return static::$categoryRegistry[$id];
	}
}
